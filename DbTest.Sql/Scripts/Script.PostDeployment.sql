﻿/*
Remove all foreign keys from tables
	while any foreign key at INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	alter the table and drop the constraint
*/
WHILE(
	EXISTS (
		SELECT * 
			FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
			WHERE CONSTRAINT_TYPE='FOREIGN KEY'
	)
)
BEGIN
	DECLARE @sql VARCHAR(2000)
	SELECT TOP 1 
	@sql=('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME+ ']' 
			+'DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'
	--PRINT @sql;
	EXEC(@sql)
END


GO

/*
Reset all tables
	truncate all tables
*/
DECLARE @schemaName VARCHAR(100), @tableName VARCHAR(100), @sql VARCHAR(400);
DECLARE DbTables CURSOR FOR 
	SELECT TABLE_SCHEMA, TABLE_NAME
		FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_TYPE = 'BASE TABLE'
				AND TABLE_NAME != 'sysdiagrams';
OPEN DbTables
WHILE 1=1
BEGIN 
    FETCH NEXT FROM DbTables INTO @schemaName, @tableName
	If @@FETCH_STATUS <> 0
	BEGIN
		BREAK;
	END
    SET @sql = 'TRUNCATE TABLE [' +@schemaName +'].[' +@tableName +']';
    --PRINT @sql;
    EXEC(@sql)
END
CLOSE DbTables
DEALLOCATE DbTables
