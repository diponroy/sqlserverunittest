﻿
                CREATE PROCEDURE  InactiveUser	
                    @id INT,	
                    @updatedBy INT	
                AS
                BEGIN
	                UPDATE [User] 
		                SET [Status] = 1,
			                UpdatedBy = @updatedBy,
			                UpdatedDateTime = GETDATE()		
		                WHERE Id = @id
                END