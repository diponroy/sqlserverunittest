﻿
                CREATE PROCEDURE  AddUser	
	                @firstName VARCHAR(100),
	                @lastName VARCHAR(100),
	                @email	VARCHAR(100),
	                @addedBy INT	
                AS
                BEGIN
	                INSERT 
		                INTO [User] (FirstName, LastName, Email, [Status], AddedBy, AddedDateTime)
		                VALUES (@firstName, @lastName, @email, 0, @addedBy, GETDATE())
	                RETURN SCOPE_IDENTITY();
                END