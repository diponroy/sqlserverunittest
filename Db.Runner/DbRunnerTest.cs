﻿using NUnit.Framework;

namespace Db.Runner
{
    [TestFixture]
    internal class DbTest
    {
        [Test]
        public void Create()
        {
            Assert.DoesNotThrow(() => new DbInitializer().DropAndCreate());
        }
    }
}
