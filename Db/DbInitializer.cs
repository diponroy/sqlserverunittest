﻿using System.Linq;
using Db.Context;
using Db.Model;

namespace Db
{
    public class DbInitializer
    {
        public void DropAndCreate()
        {
            using (var db = new UmsDbContext())
            {
                db.Set<User>().ToList();
                db.Set<Address>().ToList();
            }
        }
    }
}
