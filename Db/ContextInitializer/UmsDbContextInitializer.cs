using System;
using System.Data.Entity;
using Db.Context;
using Db.Enum;
using Db.Model;

namespace Db.ContextInitializer
{
    internal class UmsDbContextInitializer : DropCreateDatabaseAlways<UmsDbContext>
    {
        protected override void Seed(UmsDbContext context)
        {
            AddUserSp(context);
            InactiveUser(context);
            RemoveUser(context);

            AddDefaultUser(context);
        }

        private void RemoveUser(UmsDbContext context)
        {
            context.Database.ExecuteSqlCommand(@"
                CREATE PROCEDURE  RemoveUser	
                    @id INT,	
                    @updatedBy INT	
                AS
                BEGIN
	                UPDATE [User] 
		                SET [Status] = 2,
			                UpdatedBy = @updatedBy,
			                UpdatedDateTime = GETDATE()		
		                WHERE Id = @id
                END
            ");
        }

        private void InactiveUser(UmsDbContext context)
        {
            context.Database.ExecuteSqlCommand(@"
                CREATE PROCEDURE  InactiveUser	
                    @id INT,	
                    @updatedBy INT	
                AS
                BEGIN
	                UPDATE [User] 
		                SET [Status] = 1,
			                UpdatedBy = @updatedBy,
			                UpdatedDateTime = GETDATE()		
		                WHERE Id = @id
                END
            ");
        }

        private void AddUserSp(UmsDbContext context)
        {
            context.Database.ExecuteSqlCommand(@"
                CREATE PROCEDURE  AddUser	
	                @firstName VARCHAR(100),
	                @lastName VARCHAR(100),
	                @email	VARCHAR(100),
	                @addedBy INT	
                AS
                BEGIN
	                INSERT 
		                INTO [User] (FirstName, LastName, Email, [Status], AddedBy, AddedDateTime)
		                VALUES (@firstName, @lastName, @email, 0, @addedBy, GETDATE())
	                RETURN SCOPE_IDENTITY();
                END
            ");
        }

        private void AddDefaultUser(UmsDbContext context)
        {
            context.Users.Add(new User()
            {
                FirstName = "Super",
                LastName = "User",
                Email = "SuperUser@super.com",
                Status = UmsEntityEnum.Active,
                AddedBy = 1,
                AddedDateTime = DateTime.Now
            });
            context.SaveChanges();
        }
    }
}