﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Db.Configuration;
using Db.ContextInitializer;
using Db.Model;

namespace Db.Context
{
    public class UmsDbContext : DbContext, IUmsDbContext
    {
        public IDbSet<User> Users { get; set; }
        public IDbSet<Address> Addresses { get; set; }

        public UmsDbContext() : base(nameOrConnectionString: "DbUMS"){}

        static UmsDbContext()
        {
            Database.SetInitializer(new UmsDbContextInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new UserConfig());
            modelBuilder.Configurations.Add(new AddressConfig());
        }
    }
}
