﻿using System;
using System.Collections.Generic;
using Db.Enum;
using Db.Model.IEntity;

namespace Db.Model
{
    public class User : IUmsEntity
    {
        public long Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public UmsEntityEnum Status { get; set; }
        public DateTime AddedDateTime { get; set; }
        public long AddedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public long? UpdatedBy { get; set; }

        public virtual User AddedByUser { get; set; }
        public virtual User UpdatedByUser { get; set; }

        public virtual List<Address> Addresses { get; set; }

    }
}
