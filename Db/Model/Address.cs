﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Db.Enum;
using Db.Model.IEntity;

namespace Db.Model
{
    public class Address : IUmsEntity
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        public string Description { get; set; }

        public UmsEntityEnum Status { get; set; }
        public DateTime AddedDateTime { get; set; }
        public long AddedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public long? UpdatedBy { get; set; }
        public virtual User AddedByUser { get; set; }
        public virtual User UpdatedByUser { get; set; }
        public virtual User User { get; set; }

    }
}
