﻿using System;
using Db.Enum;

namespace Db.Model.IEntity
{
    internal interface IUmsEntity
    {
        long Id { get; set; }
        UmsEntityEnum Status { get; set; }
        DateTime AddedDateTime { get; set; }
        long AddedBy { get; set; }
        DateTime? UpdatedDateTime { get; set; }
        long? UpdatedBy { get; set; }

        User AddedByUser { get; set; }
        User UpdatedByUser { get; set; }
    }
}
