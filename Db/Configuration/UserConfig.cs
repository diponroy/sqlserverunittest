using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Db.Model;

namespace Db.Configuration
{
    public class UserConfig : EntityTypeConfiguration<User>
    {
        public UserConfig()
        {
            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Status)
                .IsRequired();

            Property(x => x.AddedBy)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired()
                .HasColumnType("DATETIME");

            Property(x => x.UpdatedDateTime)
                .HasColumnType("DATETIME");

            /*foreign keys*/
            HasRequired(x => x.AddedByUser)
                .WithMany()
                .HasForeignKey(f => f.AddedBy)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.UpdatedByUser)
                .WithMany()
                .HasForeignKey(f => f.UpdatedBy)
                .WillCascadeOnDelete(false);
        }
    }
}