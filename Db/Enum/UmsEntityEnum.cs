﻿namespace Db.Enum
{
    public enum UmsEntityEnum
    {
        Active = 0,
        Inactive = 1,
        Removed = 2
    }
}
