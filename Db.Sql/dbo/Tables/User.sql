﻿CREATE TABLE [dbo].[User] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [FirstName]       NVARCHAR (MAX) NULL,
    [LastName]        NVARCHAR (MAX) NULL,
    [Email]           NVARCHAR (MAX) NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.User_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.User_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[User]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[User]([UpdatedBy] ASC);

