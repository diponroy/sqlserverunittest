﻿CREATE TABLE [dbo].[Address] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [UserId]          BIGINT         NOT NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Address_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Address_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Address_dbo.User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[Address]([UpdatedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[Address]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[Address]([UserId] ASC);

